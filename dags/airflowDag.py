"""
Step 1 import the class and Operators
"""

from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.bash import BashOperator
from datetime import datetime
from random import randint  # Import to generate random numbers

"""
Operators are the task/Node and the edges are the dependences for the Airflow DAG 
between all o√cvx f qw1 q2f your Operators
e.g task A >> taskB -> this means that taskB is downstream of task A
    task A << taskB - > this means that task A is upstream of taskB
The datetime class is used to specify the start date for DAG
"""

"""
Step2: Create the Airflow Object
"""

"""
    Step3: Add your task!
"""
# Tasks are implemented under the dag object


def _training_model():
    return randint(1, 10)  # return an integer between 1 - 10


def _choosing_best_model(ti):
    accuracies = ti.xcom_pull(
        task_ids=["training_model_A", "training_model_B", "training_model_C"]
    )
    if max(accuracies) > 8:
        return "accurate"
    return "inaccurate"


with DAG(
    "my_dag",  # Dag id and this unqiue for identifying the DAG throughout
    # start date, the 1st of January 2021
    start_date=datetime(2021, 5, 22),
    # Cron expression, here it is a preset of Airflow, @daily means once every day.
    schedule_interval="@daily",
    catchup=False,  # This prevent the backfilling automatically from triggeering  DAG runs which reult in multiple runs
) as dag:
    """
    training_model_A = PythonOperator(
        task_id="training_model_A",
        python_callable=_training_model 
    )

    training_model_B = PythonOperator(
        task_id="training_model_B",
        python_callable=_training_model
    )

    training_model_C = PythonOperator(
        task_id="training_model_C",
        python_callable=_training_model
    )
    """
    # Alternative aprroach is using a list comprehension
    training_model_tasks = [
        PythonOperator(
            task_id=f"training_model_{model_id}",
            python_callable=_training_model,
            # this allows us to define the variable model_id above,if not define we will get the error key not define
            op_kwargs={"model": model_id},
        )
        for model_id in ["A", "B", "C"]
    ]

    """
    Choosing best model The BranchPythonOperator : It allows you to execute one task or another based on a condition, a value, a criterion
    """
    choosing_best_model = BranchPythonOperator(
        task_id="choosing_best_model", python_callable=_choosing_best_model
    )

    # This call a Bash operator.
    accurate = BashOperator(
        task_id="accurate",
        bash_command="echo 'accurate'"
    )

    inaccurate = BashOperator(
        task_id="inaccurate",
        bash_command=" echo 'inaccurate'"
    )

    training_model_tasks >> choosing_best_model >> [accurate, inaccurate]
