Apache->https://airflow.apache.org/docs/apache-airflow/2.0.1/start/docker.html
Have your docker engine installed
Have docker compose running
Create a repo - airflow-docker
cd airflow-docker

Run curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.0.1/docker-compose.yaml'

This file contains several service definitions:
---------------------------------------------------
- airflow-scheduler - The scheduler monitors all tasks and DAGs, then triggers the task instances once their dependencies are complete.
- airflow-webserver - The webserver available at http://localhost:8080.
- airflow-worker - The worker that executes the tasks given by the scheduler.
- airflow-init - The initialization service.
- flower - The flower app for monitoring the environment. It is available at http://localhost:8080.
- postgres - The database.
- redis - The redis - broker that forwards messages from scheduler to worker.

Create the necessary files
mkdir ./dags ./logs ./plugins

Run  echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env

On all operating system, you need to run database migrations and create the first user account. To do it, run

docker-compose up airflow-init

After initialization is complete, you should see a message like below.
----------------------------------------------------------------------
airflow-init_1       | Upgrades done
airflow-init_1       | Admin user airflow created
airflow-init_1       | 2.0.1
start_airflow-init_1 exited with code 0

=====================
To start Airflow
=====================

docker-compose up



======================
Cleaning up
To stop and delete containers, delete volumes with database data and download images, run:
======================================
CMD>>
docker-compose down --volumes --rmi all
