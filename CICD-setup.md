Set Ubuntu EC2 instance
ELevate the permission for pem key >> Chmod 700 tetx.pem
Install docker and docker compose
Install gitlab runner on the machine
  - # Linux x86-64
    sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
Give irt permission to execute
- sudo chmod +x /usr/local/bin/gitlab-runner

Register the Runner >https://docs.gitlab.com/runner/register/index.html#linux
this is how gitlab and the ec2 can communicate
- sudo gitlab-runner register
- prompts for GITlab url ->https://gitlab.com/

Select executor 
- custom, docker, docker-ssh, shell, docker+machine, kubernetes, parallels, ssh, virtualbox, docker-ssh+machine: Shell


Install runner & User

sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash 

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner & sudo gitlab-runner start


Setup Variables needed to execute the jobs 
  Types of Variables 
  - Protected -> Only exposed to protected brances
  - Masked -> Hidden in jobs and Must match requirement
  For Gitlab we are going to set six variables
   - _AIRFLOW_WWW_USER_PASSWORD
   - _AIRFLOW_WWW_USER_USERNAME
   - EC2_ADDRESS
   - SSH_KEY_EC2 -> value should be inputted as a file
   - GITLAB_PASSWORD 
   - GITLAB_USERNAME
   