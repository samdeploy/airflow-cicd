======================================
To get list of dags using API CALL
======================================
CMD >>
    ENDPOINT_URL="http://localhost:8080/"
    curl -X GET  \
        --user "airflow:airflow" \
        "${ENDPOINT_URL}/api/v1/pools or dags"



======================================
If you have Linux or Mac OS, you can make your work easier and download a optional wrapper scripts that will allow you to run commands with a simpler command.
========================================

CMD>>
- docker-compose run airflow-worker airflow info

- curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.1.0/airflow.sh'
- chmod +x airflow.sh

- ./airflow.sh info

- ./airflow.sh bash

- ./airflow.sh python




======================
Apache Airflow DAG
======================
pip install apache-airflow

There are 4 steps to follow to create a data pipeline
- Make the Imports
- Create the Airflow DAG object

=============
Crontab guru
=============

https://crontab.guru/#5_4_4_*_*


==================================
Apache air flow job business case
==================================

https://python.plainenglish.io/simple-etl-with-airflow-372b0109549



===============================
Airflow url 
===============================
http://localhost:8080/graph?dag_id=my_dag